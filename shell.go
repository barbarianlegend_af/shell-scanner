// Package shell supports splitting and joining of shell command strings.
//
// The Split function divides a string into whitespace-separated fields,
// respecting single and double quotation marks as defined by the Shell Command
// Language section of IEEE Std 1003.1 2013.  The Quote function quotes
// characters that would otherwise be subject to shell evaluation, and the Join
// function concatenates quoted strings with spaces between them.
//
// The relationship between Split and Join is that given
//
//     fields, ok := Split(Join(ss))
//
// the following relationship will hold:
//
//     fields == ss && ok
//
package shell

import (
	"bufio"
	"bytes"
	"io"
	"strings"
)

// These characters must be quoted to escape special meaning.  This list
// doesn't include the single quote.
const mustQuote = "|&;<>()$`\\\"\t\n"

// These characters should be quoted to escape special meaning, since in some
// contexts they are special (e.g., "x=y" in command position, "*" for globs).
const shouldQuote = `*?[#~=%`

// These are the separator characters in unquoted text.
const spaces = " \t\n"

const allQuote = mustQuote + shouldQuote + spaces

type mode byte

const (
	standard mode = iota
	raw
)

type state int

const (
	stNone state = iota
	stBreak
	stBreakQ
	stWord
	stWordQ
	stSingle
	stDouble
	stDoubleQ
)

// Must contain all states.
var states = [...]state{
	stNone,
	stBreak,
	stBreakQ,
	stWord,
	stWordQ,
	stSingle,
	stDouble,
	stDoubleQ,
}

type class int

const (
	clOther class = iota
	clBreak
	clNewline
	clQuote
	clSingle
	clDouble
)

type action int

const (
	drop action = iota
	push
	xpush
	emit
	remit
	rqemit
)

type lookup struct {
	state
	action
}

var classOf = [256]class{
	' ':  clBreak,
	'\t': clBreak,
	'\n': clNewline,
	'\\': clQuote,
	'\'': clSingle,
	'"':  clDouble,
}

type Token struct {
	Text  string
	Raw   string
	Quote byte
}

// A Scanner partitions input from a reader into tokens divided on space, tab,
// and newline characters.  Single and double quotation marks are handled as
// described in http://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_02.
type Scanner struct {
	mode   mode
	update [len(states)][]lookup
	buf    *bufio.Reader
	tok    bytes.Buffer
	raw    bytes.Buffer
	quote  byte
	carry  byte // carries over quotes from last token for raw capture
	st     state
	err    error
}

// NewScanner returns a Scanner that reads input from r.
func NewScanner(r io.Reader) *Scanner {
	return newScanner(standard, r)
}

// NewScanner returns a Scanner that reads input from r.
func NewRawScanner(r io.Reader) *Scanner {
	return newScanner(raw, r)
}

func newScanner(mode mode, r io.Reader) *Scanner {
	var update [len(states)][]lookup
	if mode == standard {
		update = updateStandard
	} else {
		update = updateRaw
	}
	return &Scanner{
		mode:   mode,
		update: update,
		buf:    bufio.NewReader(r),
		st:     stBreak,
	}
}

// Next advances the scanner and reports whether there are any further tokens
// to be consumed.
func (s *Scanner) Next() bool {
	if s.err != nil {
		return false
	}
	s.tok.Reset()
	s.raw.Reset()
	s.quote = 0
	if s.carry != 0 {
		s.raw.WriteByte(s.carry)
		if isQuote(s.carry) {
			s.quote = s.carry
		}
		s.carry = 0
	}
	for {
		c, err := s.buf.ReadByte()
		s.err = err
		if err == io.EOF {
			if s.mode == raw {
				return true
			}
			break
		} else if err != nil {
			return false
		}
		next := s.update[s.st][classOf[c]]
		s.st = next.state
		switch next.action {
		case push:
			s.tok.WriteByte(c)
			s.raw.WriteByte(c)
		case xpush:
			s.tok.Write([]byte{'\\', c})
			s.raw.WriteByte(c)
		case drop:
			s.raw.WriteByte(c)
			if isQuote(c) {
				s.quote = c
			}
			continue
		case emit:
			s.raw.WriteByte(c)
			return true
		case remit:
			s.buf.UnreadByte()
			return true
		case rqemit:
			s.carry = c
			return true
		default:
			panic("unknown action")
		}
	}
	return s.st != stBreak
}

func isQuote(c byte) bool {
	return classOf[c] == clSingle || classOf[c] == clDouble
}

// Token returns the image of the current token, which may be "" if there is none.
func (s *Scanner) Token() Token { return Token{s.tok.String(), s.raw.String(), s.quote} }

// Err returns the error, if any, that resulted from the most recent action.
func (s *Scanner) Err() error { return s.err }

// Complete reports whether the current token is complete, meaning that it is
// unquoted or its quotes were balanced.
func (s *Scanner) Complete() bool { return s.st == stBreak || s.st == stWord }

// Rest returns an io.Reader for the remainder of the unconsumed input in s.
// After calling this method, Next will always return false.  The remainder
// does not include the text of the current token at the time Rest is called.
func (s *Scanner) Rest() io.Reader {
	s.st = stNone
	s.tok.Reset()
	s.raw.Reset()
	s.quote = 0
	s.err = io.EOF
	return s.buf
}

// Each calls f for each token in the scanner until the input is exhausted, f
// returns false, or an error occurs.
func (s *Scanner) Each(f func(Token) bool) error {
	for s.Next() {
		if !f(s.Token()) {
			return nil
		}
	}
	if err := s.Err(); err != io.EOF {
		return err
	}
	return nil
}

// Split returns the remaining tokens in s, not including the current token if
// there is one.  Any tokens already consumed are still returned, even if there
// is an error.
func (s *Scanner) Split() []Token {
	var tokens []Token
	for s.Next() {
		tokens = append(tokens, s.Token())
	}
	return tokens
}

// Split partitions s into tokens divided on space, tab, and newline characters
// using a *Scanner.  Leading and trailing whitespace are ignored.
//
// The Boolean flag reports whether the final token is "valid", meaning there
// were no unclosed quotations in the string.
func Split(s string) ([]Token, bool) {
	return split(NewScanner(strings.NewReader(s)), s)
}

func SplitRaw(s string) ([]Token, bool) {
	return split(NewRawScanner(strings.NewReader(s)), s)
}

func split(sc *Scanner, s string) ([]Token, bool) {
	ss := sc.Split()
	return ss, sc.Complete()
}

func Quotable(s string) (hasQ, hasOther bool) {
	const (
		quote = 1
		other = 2
		all   = quote + other
	)
	var v uint
	for i := 0; i < len(s) && v < all; i++ {
		if s[i] == '\'' {
			v |= quote
		} else if strings.IndexByte(allQuote, s[i]) >= 0 {
			v |= other
		}
	}
	return v&quote != 0, v&other != 0
}

// Quote returns a copy of s in which shell metacharacters are quoted to
// protect them from evaluation.
func Quote(s string) string {
	var buf bytes.Buffer
	return quote(s, &buf)
}

// quote implements quotation, using the provided buffer as scratch space.  The
// existing contents of the buffer are clobbered.
func quote(s string, buf *bytes.Buffer) string {
	if s == "" {
		return "''"
	}
	hasQ, hasOther := Quotable(s)
	if !hasQ && !hasOther {
		return s // fast path: nothing needs quotation
	}

	buf.Reset()
	inq := false
	for i := 0; i < len(s); i++ {
		ch := s[i]
		if ch == '\'' {
			if inq {
				buf.WriteByte('\'')
				inq = false
			}
			buf.WriteByte('\\')
		} else if !inq && hasOther {
			buf.WriteByte('\'')
			inq = true
		}
		buf.WriteByte(ch)
	}
	if inq {
		buf.WriteByte('\'')
	}
	return buf.String()
}

// Join quotes each element of ss with Quote and concatenates the resulting
// strings separated by spaces.
func Join(ss []string) string {
	quoted := make([]string, len(ss))
	var buf bytes.Buffer
	for i, s := range ss {
		quoted[i] = quote(s, &buf)
	}
	return strings.Join(quoted, " ")
}

// Unsplit reconstitutes the original input from the tokens in ts.
func Unsplit(ts []Token) string {
	b := strings.Builder{}
	for _, t := range ts {
		b.WriteString(t.Raw)
	}
	return b.String()
}
