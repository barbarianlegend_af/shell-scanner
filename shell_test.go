package shell

import (
	"fmt"
	"io"
	"log"
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestQuote(t *testing.T) {
	type testCase struct{ in, want string }
	tests := []testCase{
		{"", "''"},            // empty is special
		{"abc", "abc"},        // nothing to quote
		{"--flag", "--flag"},  // "
		{"'abc", `\'abc`},     // single quote only
		{"abc'", `abc\'`},     // "
		{`shan't`, `shan\'t`}, // "
		{"--flag=value", `'--flag=value'`},
		{"a b\tc", "'a b\tc'"},
		{`a"b"c`, `'a"b"c'`},
		{`'''`, `\'\'\'`},
		{`\`, `'\'`},
		{`'a=b`, `\''a=b'`},   // quotes and other stuff
		{`a='b`, `'a='\''b'`}, // "
		{`a=b'`, `'a=b'\'`},   // "
	}
	// Verify that all the designated special characters get quoted.
	for _, c := range shouldQuote + mustQuote {
		tests = append(tests, testCase{
			in:   string(c),
			want: fmt.Sprintf(`'%c'`, c),
		})
	}

	for _, test := range tests {
		got := Quote(test.in)
		if got != test.want {
			t.Errorf("Quote %q: got %q, want %q", test.in, got, test.want)
		}
	}
}

const (
	qz = byte(0)
	qs = '\''
	qd = '"'
)

var (
	zz = Token{"", "", qz}
	ws = Token{" ", " ", qz}
	nl = Token{"\n", "\n", qz}
)

func TestSplitStandard(t *testing.T) {
	tests := []struct {
		in   string
		want []Token
		ok   bool
	}{
		// Variations of empty input yield an empty split.
		{"", nil, true},
		{"   ", nil, true},
		{"\t", nil, true},
		{"\n ", nil, true},

		// Various escape sequences work properly.
		{`\ `, []Token{{" ", `\ `, qz}}, true},
		{`a\ `, []Token{{"a ", `a\ `, qz}}, true},
		{`\\a`, []Token{{`\a`, `\\a`, qz}}, true},
		{`"a\"b"`, []Token{{`a"b`, `"a\"b"`, qd}}, true},
		{`'\'`, []Token{{`\`, `'\'`, qs}}, true},

		// Leading and trailing whitespace are discarded correctly.
		{"a", []Token{{"a", "a", qz}}, true},
		{" a", []Token{{"a", " a", qz}}, true},
		{"a\n", []Token{{"a", "a\n", qz}}, true},

		// Escaped newlines are magic in the correct ways.
		{"a\\\nb", []Token{{"ab", "a\\\nb", qz}}, true},
		{"a \\\n  b\tc", []Token{{"a", "a ", qz}, {"b", "\\\n  b\t", qz}, {"c", "c", qz}}, true},

		// Various splits with and without quotes.  Quoted whitespace is
		// preserved.
		{"a b c", []Token{{"a", "a ", qz}, {"b", "b ", qz}, {"c", "c", qz}}, true},
		{`a 'b c'`, []Token{{"a", "a ", qz}, {"b c", "'b c'", qs}}, true},
		{"\"a\nb\"cd e'f'", []Token{{"a\nbcd", "\"a\nb\"cd ", qd}, {"ef", "e'f'", qs}}, true},
		{"'\n \t '", []Token{{"\n \t ", "'\n \t '", qs}}, true},

		// Quoted empty strings are preserved in various places.
		{"''", []Token{{"", "''", qs}}, true},
		{"a ''", []Token{{"a", "a ", qz}, {"", "''", qs}}, true},
		{" a \"\" b ", []Token{{"a", " a ", qz}, {"", `"" `, qd}, {"b", "b ", qz}}, true},
		{"'' a", []Token{{"", "'' ", qs}, {"a", "a", qz}}, true},

		// Unbalanced quotation marks and escapes are detected.
		{"\\", []Token{{"", `\`, qz}}, false},                         // escape without a target
		{"'", []Token{{"", "'", qs}}, false},                          // unclosed single
		{`"`, []Token{{"", `"`, qd}}, false},                          // unclosed double
		{`'\''`, []Token{{`\`, `'\''`, qs}}, false},                   // unclosed connected double
		{`"\\" '`, []Token{{`\`, `"\\" `, qd}, {``, `'`, qs}}, false}, // unclosed separate single
		{"a 'b c", []Token{{"a", "a ", qz}, {"b c", "'b c", qs}}, false},
		{`a "b c`, []Token{{"a", "a ", qz}, {"b c", `"b c`, qd}}, false},
		{`a "b \"`, []Token{{"a", "a ", qz}, {`b "`, `"b \"`, qd}}, false},
	}
	for _, test := range tests {
		got, ok := Split(test.in)
		if ok != test.ok {
			t.Errorf("Split %#q: got valid=%v, want %v", test.in, ok, test.ok)
		}
		if diff := cmp.Diff(test.want, got); diff != "" {
			t.Errorf("Split %#q: (-want, +got)\n%s", test.in, diff)
		}
	}
}

func TestSplitRaw(t *testing.T) {
	tests := []struct {
		in   string
		want []Token
		ok   bool
	}{
		// Variations of empty input yield an empty split.
		{"", []Token{zz}, true},

		// In raw mode, leading and trailing whitespace is preserved.
		{"   ", []Token{{"   ", "   ", qz}}, true},
		{"\t", []Token{{"\t", "\t", qz}}, true},
		{"\n ", []Token{{"\n ", "\n ", qz}}, true},
		{"  \na\n  b  \n", []Token{{"  \n", "  \n", qz}, {"a", "a", qz}, {"\n  ", "\n  ", qz}, {"b", "b", qz}, {"  \n", "  \n", qz}}, true},

		// Various escape sequences work properly.
		{`\ `, []Token{{" ", `\ `, qz}}, true},
		{`a\ `, []Token{zz, {"a ", `a\ `, qz}}, true},
		{`\\a`, []Token{{`\a`, `\\a`, qz}}, true},
		{`"a\"b"`, []Token{zz, {`a"b`, `"a\"b"`, qd}}, true},
		{`'\'`, []Token{zz, {`\`, `'\'`, qs}}, true},

		// // Leading and trailing whitespace are discarded correctly.
		{"a", []Token{zz, {"a", "a", qz}}, true},
		{" a", []Token{ws, {"a", "a", qz}}, true},
		{"a\n", []Token{zz, {"a", "a", qz}, nl}, true},

		// Escaped newlines are magic in the correct ways.
		{"a\\\nb", []Token{zz, {"a\nb", "a\\\nb", qz}}, true},
		{"a \\\n  b\tc", []Token{zz, {"a", "a", qz}, {" \n  ", " \\\n  ", qz}, {"b", "b", qz}, {"\t", "\t", qz}, {"c", "c", qz}}, true},

		// Various splits with and without quotes.  Quoted whitespace is
		// preserved.
		{"a b c", []Token{zz, {"a", "a", qz}, ws, {"b", "b", qz}, ws, {"c", "c", qz}}, true},
		{`a 'b c'`, []Token{zz, {"a", "a", qz}, ws, {"b c", "'b c'", qs}}, true},
		{"\"a\nb\"cd e'f'", []Token{zz, {"a\nbcd", "\"a\nb\"cd", qd}, ws, {"ef", "e'f'", qs}}, true},
		{"'\n \t '", []Token{zz, {"\n \t ", "'\n \t '", qs}}, true},

		// Quoted empty strings are preserved in various places.
		{"''", []Token{zz, {"", "''", qs}}, true},
		{"a ''", []Token{zz, {"a", "a", qz}, ws, {"", "''", qs}}, true},
		{" a \"\" b ", []Token{ws, {"a", "a", qz}, ws, {"", `""`, qd}, ws, {"b", "b", qz}, ws}, true},
		{"'' a", []Token{zz, {"", "''", qs}, ws, {"a", "a", qz}}, true},

		// Unbalanced quotation marks and escapes are detected.
		{"\\", []Token{{"", `\`, qz}}, false},                                // escape without a target
		{"'", []Token{zz, {"", "'", qs}}, false},                             // unclosed single
		{`"`, []Token{zz, {"", `"`, qd}}, false},                             // unclosed double*
		{`'\''`, []Token{zz, {`\`, `'\''`, qs}}, false},                      // unclosed connected double
		{`"\\" '`, []Token{zz, {`\`, `"\\"`, qd}, ws, {``, `'`, qs}}, false}, // unclosed separate single
		{"a 'b c", []Token{zz, {"a", "a", qz}, ws, {"b c", "'b c", qs}}, false},
		{`a "b c`, []Token{zz, {"a", "a", qz}, ws, {"b c", `"b c`, qd}}, false},
		{`a "b \"`, []Token{zz, {"a", "a", qz}, ws, {`b "`, `"b \"`, qd}}, false},
	}
	for _, test := range tests {
		got, ok := SplitRaw(test.in)
		if ok != test.ok {
			t.Errorf("Split %#q: got valid=%v, want %v", test.in, ok, test.ok)
		}
		if diff := cmp.Diff(test.want, got); diff != "" {
			t.Errorf("Split %#q: (-want, +got)\n%s", test.in, diff)
		}
	}
}

func TestScannerSplit(t *testing.T) {
	tests := []struct {
		in         string
		want, rest []Token
	}{
		{"", nil, nil},
		{" ", nil, nil},
		{"--", nil, nil},
		{"a -- b", []Token{{"a", "a ", qz}}, []Token{{"b", "b", qz}}},
		{"a b c -- d -- e ", []Token{{"a", "a ", qz}, {"b", "b ", qz}, {"c", "c ", qz}}, []Token{{"d", "d ", qz}, {"--", "-- ", qz}, {"e", "e ", qz}}},
		{`"a b c --" -- "d "`, []Token{{"a b c --", `"a b c --" `, qd}}, []Token{{"d ", `"d "`, qd}}},
		{` -- "foo`, nil, []Token{{"foo", `"foo`, qd}}}, // unterminated
		{"cmd -flag -- arg1 arg2", []Token{{"cmd", "cmd ", qz}, {"-flag", "-flag ", qz}}, []Token{{"arg1", "arg1 ", qz}, {"arg2", "arg2", qz}}},
	}
	for _, test := range tests {
		t.Logf("Scanner split input: %q", test.in)

		s := NewScanner(strings.NewReader(test.in))
		var got, rest []Token
		for s.Next() {
			if s.Token().Text == "--" {
				rest = s.Split()
				break
			}
			got = append(got, s.Token())
		}

		if s.Err() != io.EOF {
			t.Errorf("Unexpected scan error: %v", s.Err())
		}

		if diff := cmp.Diff(test.want, got); diff != "" {
			t.Errorf("Scanner split prefix: (-want, +got)\n%s", diff)
		}
		if diff := cmp.Diff(test.rest, rest); diff != "" {
			t.Errorf("Scanner split suffix: (-want, +got)\n%s", diff)
		}
	}
}

func TestRoundTrip(t *testing.T) {
	tests := [][]string{
		{},
		{"a"},
		{"a "},
		{"a", "b", "c"},
		{"a", "b c"},
		{"--flag=value"},
		{"m='$USER'", "nop+", "$$"},
		{`"a" b `, "c"},
		{"odd's", "bodkins", "x'", "x''", `x""`, "$x':y"},
		{"a=b", "--foo", "${bar}", `\$`},
		{"cat", "a${b}.txt", "|", "tee", "capture", "2>", "/dev/null"},
	}
	for _, test := range tests {
		s := Join(test)
		t.Logf("Join %#q = %v", test, s)
		got, ok := Split(s)
		if !ok {
			t.Errorf("Split %+q: should be valid, but is not", s)
		}
		gots := []string{}
		for _, t := range got {
			gots = append(gots, t.Text)
		}
		if diff := cmp.Diff(test, gots); diff != "" {
			t.Errorf("Split %+q: (-want, +got)\n%s", s, diff)
		}
	}
}

func TestUnsplit(t *testing.T) {
	tests := []struct {
		in string
		ok bool
	}{
		{"", true},
		{" ", true},
		{"\t", true},
		{"\n", true},
		{" \t\n", true},
		{"a", true},
		{"a ", true},
		{"a   ", true},
		{"a\n", true},
		{" a \n b ", true},
		{"a b c", true},
		{"a 'b c'", true},
		{"a 'b c", false},
		{"a \"b c\"", true},
		{"a \"b c", false},
		{"--flag=value", true},
		{"m='$USER' nop+ $$", true},
		{`"a" b c`, true},
		{"'odd\\'s' bodkins 'x\\' \"x''\" 'x\"\"' \"$x':y\"", true},
		{"a=b --foo ${bar} \\$", true},
		{"cat a${b}.txt | tee capture 2> /dev/null", true},
	}
	for _, test := range tests {
		split, ok := SplitRaw(test.in)
		got := Unsplit(split)
		if diff := cmp.Diff(test.in, got); diff != "" {
			t.Errorf("Split %+q: (-want, +got)\n%s", test.in, diff)
		}
		if ok != test.ok {
			t.Errorf("Split %+q: got valid=%v, want %v", test.in, ok, test.ok)
		}
	}
}

func ExampleScanner() {
	const input = `a "free range" exploration of soi\ disant novelties`
	s := NewScanner(strings.NewReader(input))
	sum, count := 0, 0
	for s.Next() {
		count++
		tok := s.Token()
		sum += len(tok.Text)
		fmt.Println(tok.Text)
	}
	fmt.Println(len(input), count, sum, s.Complete(), s.Err())
	// Output:
	// a
	// free range
	// exploration
	// of
	// soi disant
	// novelties
	// 51 6 43 true EOF
}

func ExampleScanner_raw() {
	const input = `a "free range" exploration of soi\ disant novelties`
	s := NewRawScanner(strings.NewReader(input))
	sum, count := 0, 0
	for s.Next() {
		count++
		tok := s.Token()
		sum += len(tok.Raw)
		fmt.Println(strings.ReplaceAll(tok.Raw, " ", "_"))
	}
	fmt.Println(len(input), count, sum, s.Complete(), s.Err())
	// Output:
	// a
	// _
	// "free_range"
	// _
	// exploration
	// _
	// of
	// _
	// soi\_disant
	// _
	// novelties
	// 51 12 51 true EOF
}

func ExampleScanner_Rest() {
	const input = `things 'and stuff' %end% all the remaining stuff`
	s := NewScanner(strings.NewReader(input))
	for s.Next() {
		if s.Token().Text == "%end%" {
			fmt.Print("found marker; ")
			break
		}
	}
	rest, err := io.ReadAll(s.Rest())
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(string(rest))
	// Output: found marker; all the remaining stuff
}

func ExampleScanner_Each() {
	const input = `a\ b 'c d' "e f's g" 	 stop "go directly to jail"`
	if err := NewScanner(strings.NewReader(input)).Each(func(tok Token) bool {
		fmt.Println(tok.Text)
		return tok.Text != "stop"
	}); err != nil {
		log.Fatal(err)
	}
	// Output:
	// a b
	// c d
	// e f's g
	// stop
}

func ExampleScanner_Each_raw() {
	const input = `a\ b 'c d' "e f's g" 	 stop "go directly to jail"`
	if err := NewRawScanner(strings.NewReader(input)).Each(func(tok Token) bool {
		fmt.Println(strings.ReplaceAll(tok.Raw, " ", "_"))
		return tok.Text != "stop"
	}); err != nil {
		log.Fatal(err)
	}
	// Output:
	// a\_b
	// _
	// 'c_d'
	// _
	// "e_f's_g"
	// _	_
	// stop
}

func ExampleScanner_Split() {
	const input = `cmd -flag=t -- foo bar baz`

	s := NewScanner(strings.NewReader(input))
	for s.Next() {
		if s.Token().Text == "--" {
			split := []string{}
			for _, t := range s.Split() {
				split = append(split, t.Text)
			}
			fmt.Println("** Args:", strings.Join(split, ", "))
		} else {
			fmt.Println(s.Token().Text)
		}
	}
	// Output:
	// cmd
	// -flag=t
	// ** Args: foo, bar, baz
}
